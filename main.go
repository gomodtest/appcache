package appcache

const version = "1.0.0"

type AppCache struct {
}

func (a *AppCache) Version() string {
	return version
}

func NewAppCache() *AppCache {
	appCache := AppCache{}

	return &appCache
}
